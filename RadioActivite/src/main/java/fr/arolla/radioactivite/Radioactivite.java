package fr.arolla.radioactivite;

import static fr.arolla.radioactivite.model.IntervalRadioactivite.JAUNE;
import static fr.arolla.radioactivite.model.IntervalRadioactivite.MARRON;
import static fr.arolla.radioactivite.model.IntervalRadioactivite.ORANGE;
import static fr.arolla.radioactivite.model.IntervalRadioactivite.ROUGE;

import fr.arolla.radioactivite.model.IntervalRadioactivite;

public class Radioactivite {

	public String classifie(int valeur) {
		String result = "";;
		result = classifiePourInterval(valeur, JAUNE, result);
		result = classifiePourInterval(valeur, ORANGE, result);
		result = classifiePourInterval(valeur, MARRON, result);
		result = classifiePourInterval(valeur, ROUGE, result);
		return result;
	}

	private String classifiePourInterval(int valeur, IntervalRadioactivite interval, String result) {
		if (valeur >= interval.getValeurBasse()) {
			result = interval.name();
		}
		return result;
	}
	
}
