/**
 * 
 */
package fr.arolla.radioactivite.model;

/**
 * @author Guillaume
 *
 */
public enum IntervalRadioactivite {
	JAUNE(0),
	ORANGE(51),
	MARRON(101),
	ROUGE(151);
	
	private int valeurBasse;

	private IntervalRadioactivite(int valeurBasse) {
		this.valeurBasse = valeurBasse;
	}

	public int getValeurBasse() {
		return valeurBasse;
	}
}
