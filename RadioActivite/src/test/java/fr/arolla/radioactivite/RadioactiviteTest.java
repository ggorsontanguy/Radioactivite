package fr.arolla.radioactivite;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RadioactiviteTest {

	private static final String ROUGE = "ROUGE";
	private static final String MARRON = "MARRON";
	private static final String ORANGE = "ORANGE";
	private static final String JAUNE = "JAUNE";

	@Test
	public void should_return_JAUNE_when_0() {
		int valeur = 0;
		String expected = JAUNE;

		testClassifie(valeur, expected);
	}

	@Test
	public void should_return_JAUNE_when_30() {
		int valeur = 30;
		String result = new Radioactivite().classifie(valeur);
		assertEquals(JAUNE, result);
	}

	@Test
	public void should_return_JAUNE_when_50() {
		int valeur = 50;
		String expected = JAUNE;

		testClassifie(valeur, expected);
	}

	@Test
	public void should_return_ORANGE_when_51() {
		int valeur = 51;
		String expected = ORANGE;

		testClassifie(valeur, expected);
	}

	@Test
	public void should_return_ORANGE_when_60() {
		int valeur = 60;
		String expected = ORANGE;

		testClassifie(valeur, expected);
	}

	@Test
	public void should_return_ORANGE_when_100() {
		int valeur = 100;
		String expected = ORANGE;

		testClassifie(valeur, expected);
	}

	@Test
	public void should_return_MARRON_when_101() {
		int valeur = 101;
		String expected = MARRON;

		testClassifie(valeur, expected);
	}

	@Test
	public void should_return_MARRON_when_150() {
		int valeur = 150;
		String expected = MARRON;

		testClassifie(valeur, expected);
	}

	@Test
	public void should_return_RED_when_151() {
		int valeur = 151;
		String expected = ROUGE;

		testClassifie(valeur, expected);
	}

	private void testClassifie(int valeur, String expected) {
		String result = new Radioactivite().classifie(valeur);
		assertEquals(expected, result);
	}
}
